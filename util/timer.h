#pragma once

#include <chrono>

class Timer {
  public:
    Timer();

    double GetDuration() const;

  private:
    std::chrono::time_point<std::chrono::system_clock> start_time;
};
