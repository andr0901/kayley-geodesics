#pragma once

#include <vector>
#include <cstdint>
#include <unordered_map>
#include <ostream>

// Hash basis
constexpr uint64_t P = 31;

class Permutation {
  public:
    Permutation() = default;

    // Constructs identity permutation of size n
    Permutation(size_t n);

    // Constructs permutation of size n with given binary representation
    Permutation(size_t n, uint64_t binary);

    Permutation(const std::vector<size_t>& vector);

    Permutation(std::initializer_list<size_t> list);

    Permutation(const Permutation& other) = default;

    Permutation operator*(const Permutation& other) const;

    bool operator==(const Permutation& other) const;

    Permutation Inverse() const;

    void Set(size_t i, size_t val);

    size_t Get(size_t i) const;

    uint64_t AsInt() const;

    std::vector<size_t> AsVector() const;

    bool NextPermutation();

    // [l, r)
    Permutation CycleShift(size_t l, size_t r) const;

  private:
    size_t n_;

    // No permutation of size >= 16 is allowed since in won't fit 64-bit integer
    uint64_t p_;

    // block_len_ = ceil(log_2(n_))
    size_t block_len_;

    // block of ones of length block_len
    uint64_t block_ones_;
};

std::ostream& operator<<(std::ostream& os, const Permutation& permutation);

std::vector<Permutation> GetPermutationsWithoutInverses(size_t n);
