#include "timer.h"

Timer::Timer() : start_time(std::chrono::high_resolution_clock::now()) {
}

double Timer::GetDuration() const {
    std::chrono::duration<float> duration = std::chrono::high_resolution_clock::now() - start_time;
    return duration.count();
}
