#include "permutation.h"

#include <algorithm>
#include <unordered_set>

Permutation::Permutation(size_t n) : n_(n), p_(0) {
    block_len_ = 0;
    size_t capacity = 1;
    while (capacity < n) {
        capacity *= 2;
        ++block_len_;
    }
    block_ones_ = (1 << block_len_) - 1;

    for (size_t i = 0; i != n_; ++i) {
        Set(i, i);
    }
}

Permutation::Permutation(size_t n, uint64_t binary) : Permutation(n) {
    p_ = binary;
}

Permutation::Permutation(const std::vector<size_t>& vector) : Permutation(vector.size()) {
    size_t i = 0;
    for (size_t x : vector) {
        Set(i++, x);
    }
}

Permutation::Permutation(std::initializer_list<size_t> list) : Permutation(list.size()) {
    size_t i = 0;
    for (size_t x : list) {
        Set(i++, x);
    }
}

Permutation Permutation::operator*(const Permutation &other) const {
    Permutation ans(n_);
    for (size_t i = 0; i != n_; ++i) {
        ans.Set(i, other.Get(Get(i)));
    }
    return ans;
}

bool Permutation::operator==(const Permutation& other) const {
    return AsInt() == other.AsInt();
}

Permutation Permutation::Inverse() const {
    Permutation inv(n_);
    for (size_t i = 0; i != n_; ++i) {
        inv.Set(Get(i), i);
    }
    return inv;
}

void Permutation::Set(size_t i, size_t val) {
    p_ &= (~0ULL ^ (block_ones_ << block_len_ * i));    // first make zeroes
    p_ |= (val << block_len_ * i);                      // then set value
}

size_t Permutation::Get(size_t i) const {
    return (p_ >> block_len_ * i) & block_ones_;
}

uint64_t Permutation::AsInt() const {
    return p_;
}

std::vector<size_t> Permutation::AsVector() const {
    uint64_t p_copy = p_;
    std::vector<size_t> vector(n_);
    for (size_t i = 0; i != n_; ++i) {
        vector[i] = (p_copy & block_ones_);
        p_copy >>= block_len_;
    }
    return vector;
}

bool Permutation::NextPermutation() {
    std::vector<size_t> p_as_vector = AsVector();
    bool ans = std::next_permutation(p_as_vector.begin(), p_as_vector.end());
    *this = Permutation(p_as_vector);
    return ans;
}

Permutation Permutation::CycleShift(size_t l, size_t r) const {
    Permutation ans = *this;
    for (size_t i = l; i + 1 < r; ++i) {
        size_t t = ans.Get(i);
        ans.Set(i, ans.Get(i + 1));
        ans.Set(i + 1, t);
    }
    return ans;
}

std::ostream& operator<<(std::ostream& os, const Permutation& permutation) {
    for (size_t x : permutation.AsVector()) {
        os << x << " ";
    }
    return os;
}

std::vector<Permutation> GetPermutationsWithoutInverses(size_t n) {
    std::unordered_set<uint64_t> used;
    std::vector<Permutation> all_permutations;
    Permutation p(n);
    do {
        if (used.contains(p.Inverse().AsInt())) {
            continue;
        }
        // do not consider identity permutation
        if (p == Permutation(n)) {
            continue;
        }
        all_permutations.push_back(p);
        used.insert(p.AsInt());
    } while (p.NextPermutation());
    return all_permutations;
}
