#include "permutation.h"

#include <gtest/gtest.h>
#include <numeric>

TEST(PermutationTest, Basic) {
    Permutation a(5);
    for (size_t i = 0; i != 5; ++i) {
        ASSERT_EQ(a.Get(i), i);
    }

    Permutation b{1, 3, 2, 4, 0};
    Permutation c = a * b;
    ASSERT_EQ(c.AsInt(), b.AsInt());
    ASSERT_NE(c.AsInt(), a.AsInt());
    for (size_t i = 0; i != 5; ++i) {
        ASSERT_EQ(c.Get(i), b.Get(i));
    }

    Permutation d = c * Permutation{2, 3, 1, 0, 4};
    ASSERT_EQ(d, Permutation({3, 0, 1, 4, 2}));
    ASSERT_EQ(d.NextPermutation(), true);
    ASSERT_EQ(d, Permutation({3, 0, 2, 1, 4}));

    Permutation e = d.Inverse();
    ASSERT_EQ(e, Permutation({1, 3, 2, 0, 4}));
}
