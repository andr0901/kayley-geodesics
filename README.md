### Run

In order to run a program, do the following:

```
*assuming you're located on the root of the project*
git submodule init
git submodule update
mkdir build
cd build
cmake ..
make
./src/kayley_geodesics_run
```
