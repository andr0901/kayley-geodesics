#include "geodesic_finder_k_subset.h"
#include "logger.h"

#include <future>
#include <mutex>
#include <algorithm>

GeodesicFinderKSubset::GeodesicFinderKSubset(size_t n) : GeodesicFinder(n) {
}

std::vector<std::vector<Permutation>> GeodesicFinderKSubset::Find(size_t k) const {
    std::vector<std::vector<Permutation>> found;
    std::mutex found_mutex;
    std::vector<std::unordered_set<uint64_t>> tasks;
    tasks.reserve(MAX_TASKS_SIZE);
    GenerateFirst(tasks, found, found_mutex, Permutation(n_), 0, n_, k);
    std::vector<std::future<void>> futures;
    for (size_t i = 0; i < tasks.size(); i += BATCH_LEN) {
        futures.emplace_back(std::async(std::launch::async,
        [this, i, &tasks, &found, &found_mutex](){
            for (size_t j = i; j < std::min(tasks.size(), i + BATCH_LEN); ++j) {
                std::vector<Permutation> generating_set;
                for (uint64_t permutation : tasks[j]) {
                    Permutation p(n_, permutation);
                    generating_set.push_back(p);
                    if (p.AsInt() != p.Inverse().AsInt()) {
                        generating_set.push_back(p.Inverse());
                    }
                }
                if (Check(generating_set) && !IsCycleOrComplete(generating_set)) {
                    std::lock_guard<std::mutex> lock(found_mutex);
                    found.push_back(generating_set);
                }
            }
        }));
    }
    for (auto& future : futures) {
        future.get();
    }
    tasks.clear();
    return found;
}

void GeodesicFinderKSubset::GenerateFirst(
    std::vector<std::unordered_set<uint64_t>>& tasks,
    std::vector<std::vector<Permutation>>& found,
    std::mutex& found_mutex,
    Permutation p,
    size_t last_free_idx,
    size_t last_cycle_len,
    size_t k
) const {
    if (last_free_idx >= n_) {
        if (p == Permutation(n_)) {
            return;
        }
        std::unordered_set<uint64_t> used = {p.AsInt()};
        RecurseSearch(tasks, used, found, found_mutex, 0, k);
        return;
    }
    for (size_t cycle_len = 1; cycle_len <= last_cycle_len; ++cycle_len) {
        if (last_free_idx + cycle_len > n_) {
            break;
        }
        Permutation next_p = p.CycleShift(last_free_idx, last_free_idx + cycle_len);
        GenerateFirst(tasks, found, found_mutex, next_p, last_free_idx + cycle_len, cycle_len, k);
    }
}

void GeodesicFinderKSubset::RecurseSearch(
    std::vector<std::unordered_set<uint64_t>>& tasks,
    std::unordered_set<uint64_t>& used,
    std::vector<std::vector<Permutation>>& found,
    std::mutex& found_mutex,
    size_t last_unused,
    size_t k) const {
    if (used.size() == k) {
        tasks.push_back(used);
        if (tasks.size() == MAX_TASKS_SIZE) {
            LOG("Processing one more batch...");
            std::vector<std::future<void>> futures;
            for (size_t i = 0; i != MAX_TASKS_SIZE; i += BATCH_LEN) {
                futures.emplace_back(std::async(std::launch::async,
                    [this, i, &tasks, &found, &found_mutex](){
                    for (size_t j = i; j != i + BATCH_LEN; ++j) {
                        std::vector<Permutation> generating_set;
                        for (uint64_t permutation : tasks[j]) {
                            Permutation p(n_, permutation);
                            generating_set.push_back(p);
                            if (p.AsInt() != p.Inverse().AsInt()) {
                                generating_set.push_back(p.Inverse());
                            }
                        }
                        if (Check(generating_set) && !IsCycleOrComplete(generating_set)) {
                            std::lock_guard<std::mutex> lock(found_mutex);
                            found.push_back(generating_set);
                        }
                    }
                }));
            }
            for (auto& future : futures) {
                future.get();
            }
            tasks.clear();
        }
        return;
    }
    if (last_unused == permutations_.size()) {
        return;
    }
    for (size_t i = last_unused; i != permutations_.size(); ++i) {
        if (used.contains(permutations_[i].AsInt()) ||
            used.contains(permutations_[i].Inverse().AsInt())) {
            continue;
        }
        used.insert(permutations_[i].AsInt());
        RecurseSearch(tasks, used, found, found_mutex, i + 1, k);
        used.erase(permutations_[i].AsInt());
    }
}