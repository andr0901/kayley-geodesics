#pragma once

#include "geodesic_finder.h"

#include <unordered_set>
#include <mutex>

class GeodesicFinderKSubset : public GeodesicFinder {
  public:
    GeodesicFinderKSubset(size_t n);

    std::vector<std::vector<Permutation>> Find(size_t k = 0) const override;

  private:
    // Generates permutations such that
    // (0 ... p_1) (p_1 + 1 ... p_2) ... (p_{n - 1} ... n - 1),
    // where lengths of these cycles are non-ascending
    void GenerateFirst(
        std::vector<std::unordered_set<uint64_t>>& tasks,
        std::vector<std::vector<Permutation>>& found,
        std::mutex& found_mutex,
        Permutation p,
        size_t last_free_idx,
        size_t last_cycle_len,
        size_t k
    ) const;

    void RecurseSearch(
        std::vector<std::unordered_set<uint64_t>>& tasks,
        std::unordered_set<uint64_t>& used,
        std::vector<std::vector<Permutation>>& found,
        std::mutex& found_mutex,
        size_t last_unused,
        size_t k) const;

    static constexpr size_t MAX_TASKS_SIZE = 1e6;
    static constexpr size_t BATCH_LEN = 5e3;
};