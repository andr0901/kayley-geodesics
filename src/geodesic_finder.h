#pragma once

#include "permutation.h"

#include <vector>

class GeodesicFinder {
  public:
    GeodesicFinder(size_t n);

    virtual std::vector<std::vector<Permutation>> Find(size_t k = 0) const = 0;

  protected:
    // True if a graph is geodetic, false otherwise
    bool Check(const std::vector<Permutation>& generating_set) const;

    bool IsCycleOrComplete(const std::vector<Permutation>& generating_set) const;

    size_t n_;
    std::vector<Permutation> permutations_;
};
