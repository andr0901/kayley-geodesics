#include "geodesic_finder.h"

#include <deque>

#include <iostream>

GeodesicFinder::GeodesicFinder(size_t n)
    : n_(n)
    , permutations_(GetPermutationsWithoutInverses(n)) {
}

bool GeodesicFinder::Check(const std::vector<Permutation>& generating_set) const {
    std::deque<Permutation> queue = {Permutation(n_)};
    std::unordered_map<uint64_t, size_t> dst;
    dst[Permutation(n_).AsInt()] = 0;
    while (!queue.empty()) {
        Permutation v = queue.front();
        uint64_t v_hash = v.AsInt();
        queue.pop_front();
        for (const Permutation& permutation : generating_set) {
            Permutation u = v * permutation;
            uint64_t u_hash = u.AsInt();
            auto it = dst.find(u_hash);
            if (it == dst.end()) {
                dst[u_hash] = dst[v_hash] + 1;
                queue.push_back(u);
            } else if (it->second == dst[v_hash] + 1) {
                return false;
            }
        }
    }
    return true;
}

bool GeodesicFinder::IsCycleOrComplete(const std::vector<Permutation>& generating_set) const {
    std::deque<Permutation> queue = {Permutation(n_)};
    std::unordered_map<uint64_t, size_t> degree;
    while (!queue.empty()) {
        Permutation v = queue.front();
        uint64_t v_hash = v.AsInt();
        queue.pop_front();
        for (const Permutation& permutation : generating_set) {
            Permutation u = v * permutation;
            uint64_t u_hash = u.AsInt();
            auto it = degree.find(u_hash);
            if (it == degree.end()) {
                queue.push_back(u);
            }
            ++degree[v_hash];
            ++degree[u_hash];
        }
    }
    bool all_two = true;
    for (const auto& [v, deg] : degree) {
        if (deg != 4) {
            all_two = false;
        }
    }
    bool all_connected = true;
    for (const auto& [v, deg] : degree) {
        if (deg != 2 * (degree.size() - 1)) {
            all_connected = false;
        }
    }
    return all_two || all_connected;
}