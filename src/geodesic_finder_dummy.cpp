#include "geodesic_finder_dummy.h"

GeodesicFinderDummy::GeodesicFinderDummy(size_t n) : GeodesicFinder(n) {
}

std::vector<std::vector<Permutation>> GeodesicFinderDummy::Find(size_t) const {
    std::vector<std::vector<Permutation>> found;
    // overflows for n >= 5, for these permutations_.size() >= 72,
    // so this does not fit in any type
    for (size_t mask = 0; mask != (1 << permutations_.size()); ++mask) {
        std::vector<Permutation> generating_set;

        size_t bit = 0;
        for (const Permutation& p : permutations_) {
            if ((mask >> bit++) & 1) {
                generating_set.push_back(p);
                if (p.AsInt() != p.Inverse().AsInt()) {
                    generating_set.push_back(p.Inverse());
                }
            }
        }

        if (Check(generating_set) && !IsCycleOrComplete(generating_set)) {
            found.push_back(generating_set);
        }
    }
    return found;
}