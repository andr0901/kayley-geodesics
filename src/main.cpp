#include "geodesic_finder_dummy.h"
#include "geodesic_finder_k_subset.h"
#include "logger.h"
#include "permutation.h"
#include "timer.h"

#include <memory>

void PrintGraph(const std::vector<Permutation>& generating_set, size_t n) {
    size_t i = 0;
    std::unordered_map<size_t, size_t> compress;
    Permutation permutation(n);
    do {
        compress[permutation.AsInt()] = i++;
    } while (permutation.NextPermutation());

    permutation = Permutation(n);
    do {
        for (const Permutation& cur : generating_set) {
            std::cout << compress[permutation.AsInt()] << " " << compress[(permutation * cur).AsInt()] << "\n";
        }
    } while (permutation.NextPermutation());
}

void RunDummy() {
    for (size_t n = 2; n != 5; ++n) {
        LOG("n: " << n);
        std::shared_ptr<GeodesicFinder> finder = std::make_shared<GeodesicFinderDummy>(n);
        std::vector<std::vector<Permutation>> found = finder->Find();
        LOG("found size: " << found.size());
        for (const std::vector<Permutation>& generating_set : found) {
            for (const Permutation& permutation : generating_set) {
                LOG(permutation);
            }
            PrintGraph(generating_set, n);
            LOG("---");
        }
        LOG("----------");
    }
}

void RunKSubset() {
    std::vector<std::pair<size_t, size_t>> parameters = {
        {5, 2},
        {5, 3},
        {5, 4},
        {5, 5},
        {6, 2},
        {6, 3},
        {6, 4},
        {7, 2},
        {7, 3},
        {8, 2},
        {8, 3},
        {9, 2},
    };
    for (const auto& [n, k] : parameters) {
        Timer timer;
        LOG("n: " << n << " k: " << k);
        std::shared_ptr<GeodesicFinder> finder = std::make_shared<GeodesicFinderKSubset>(n);
        std::vector<std::vector<Permutation>> found = finder->Find(k);
        LOG("found size: " << found.size());
        for (const std::vector<Permutation> &generating_set : found) {
            for (const Permutation &permutation : generating_set) {
                LOG(permutation);
            }
            PrintGraph(generating_set, n);
            LOG("---");
        }
        LOG(timer.GetDuration());
        LOG("----------");
    }
}

int main() {
    RunKSubset();
    return 0;
}
