#pragma once

#include "geodesic_finder.h"

class GeodesicFinderDummy : public GeodesicFinder {
  public:
    GeodesicFinderDummy(size_t n);

    std::vector<std::vector<Permutation>> Find(size_t k = 0) const override;
};